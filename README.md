## Quandl-Node

### Description

Node module to interact with the [Quandl](https://www.quandl.com/) API.
with support for promises.
For a list of possible parameters and filters check out the official
[API Docs](https://docs.quandl.com/docs/getting-started) for Quandl

### Installation
```javascript
npm install quandl-node 
```

### How to

```javascript
const Quandl = require("quandl-node")
let quandl = new Quandl({
key:"Your-Api-key"
});
```

#### Fetching timeseries data

```javascript
let data:any = await quandl.timeseries.data({
                database:"WIKI",
                dataset:"FB",
                start_date:"2017-06-12",
                end_date: "2017-12-12",
                order:"asc",
                limit:15
            });
```

#### Fetching timeseries metadata

```javascript
 let data:any = await quandl.timeseries.metadata({
                database:"WIKI"
            });
```

### Fetching timeseries data and metadata

```javascript
let data:any = await quandl.timeseries.all({
                database:"WIKI",
                dataset:"FB",
                start_date:"2017-06-12",
                end_date: "2017-12-12",
                order:"asc",
                limit:15
            });
```

### Fetching table data 

```javascript
let data:any = await quandl.table.rows({
                publisher:"ETFG",
                datatable:"FUND",
                filter: {
                    ticker:"SPY"
                }
            });
```

### Fetching table metadata

```javascript
let data:any = await quandl.table.metadata({
               publisher:"AR",
               datatable:"MWCS"
           });
```

