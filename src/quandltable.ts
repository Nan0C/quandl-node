import {Quandlmodel} from "./quandlmodel";
import {RequestResponse} from "request";
import {fetch} from "./request";

export interface TableDataOptions {
    publisher:string,
    datatable:string,
    format?:string,
    qopts?: {
        columns?:string,
        export?:boolean,
        per_page?:number,
        cursor_id?:string
    },
    filter?:{[id:string]:any}
}

export interface TableMetadataOptions {
    publisher:string,
    datatable:string,
    format?:string
}

export class QuandlTable {
    private model:Quandlmodel;

    constructor(model:Quandlmodel){
        this.model = model;
    }

    /**
     * Get Data from a Table
     * @param {TableDataOptions} options
     * @returns {Promise<request.RequestResponse>}
     */
    rows(options:TableDataOptions):Promise<RequestResponse>{
        options.qopts = options.qopts || {};
        let config = {
            uri: [
                this.model.uri,
                "api",
                this.model.api_version,
                "datatables",
                options.publisher,
                options.datatable+"."+(options.format||"json")
            ].join('/'),
            qs: {
                api_key: this.model.api_key,
                "qopts.columns": options.qopts.columns,
                "qopts.export":options.qopts.export,
                "qopts.per_page":options.qopts.per_page,
                "qopts.cursor_id":options.qopts.cursor_id
            }
        };
        if(options.filter) {
            for (let id in options.filter) {
                config.qs[id] = options.filter[id];
            }
        }
        return fetch(config,this.model);
    }

    /**
     * Get metadata for a table
     * @param {TableMetadataOptions} options
     * @returns {Promise<request.RequestResponse>}
     */
    metadata(options:TableMetadataOptions):Promise<RequestResponse>{
        let config = {
            uri: [
                this.model.uri,
                "api",
                this.model.api_version,
                "datatables",
                options.publisher,
                options.datatable,
                "metadata"+"."+(options.format||"json")
            ].join('/'),
            qs: {
                api_key: this.model.api_key,
            }
        };
        return fetch(config,this.model);
    }

}