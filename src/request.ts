import {Quandlmodel} from "./quandlmodel";
import {RequestResponse} from "request";
import * as request from "request-promise-native";

export type RequestOptions = {
    uri:string;
    qs:{[id:string]:any},
    transform?:(body,response,resolveWithFillResponse)=>string;
    method?:string;
    proxy?:string;
}

export async function fetch(config:RequestOptions,model:Quandlmodel):Promise<RequestResponse>{
    let options = {
        uri:config.uri||model.uri,
        method:config.method||"GET",
        qs: config.qs || {},
        transform: config.transform,
        proxy: config.proxy || model.proxy
    };
    return request(options);
}