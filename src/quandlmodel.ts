/**
 * Created by Soeren on 14.02.2018.
 */

export interface QuandlConfig {
    key:string;
    api?:number;
    proxy?:string;
    url?:string;
}

export class Quandlmodel {
    private _uri:string;
    private _api_key:string;
    private _api_version:string;
    private _proxy:string;

    constructor(config:QuandlConfig){
        this._api_key = config.key || undefined;
        this._api_version = "v"+(config.api || 3);
        this._proxy = config.proxy || undefined;
        this._uri = config.url || "https://www.quandl.com";
    }

    configure(config?:QuandlConfig){
        this._api_key = config.key || this._api_key;
        this._api_version = "v"+(config.api || 3);
        this._proxy = config.proxy || this._proxy;
        this._uri = config.url || this._uri;
    }

    public get api_key():string {
        return this._api_key;
    }

    public set api_key(value:string) {
        this._api_key = value;
    }

    public get api_version():string {
        return this._api_version;
    }

    public set api_version(value:string) {
        this._api_version = value;
    }

    public get proxy():string {
        return this._proxy;
    }

    public set proxy(value:string) {
        this._proxy = value;
    }

    public get uri():string {
        return this._uri;
    }

    public set uri(value:string) {
        this._uri = value;
    }
}