import {QuandlConfig, Quandlmodel} from "./quandlmodel";
import {QuandlTimeseries} from "./quandltimeseries";
import {QuandlTable} from "./quandltable";

export class Quandl {
    private _model:Quandlmodel;
    private _timeseries:QuandlTimeseries;
    private _table:QuandlTable;

    constructor(config:QuandlConfig){
        this._model = new Quandlmodel(config);
        this._timeseries = new QuandlTimeseries(this._model);
        this._table = new QuandlTable(this._model);
    }

    public get timeseries():QuandlTimeseries {
        return this._timeseries;
    }

    public set timeseries(value:QuandlTimeseries) {
        this._timeseries = value;
    }

    public get model():Quandlmodel {
        return this._model;
    }

    public set model(value:Quandlmodel) {
        this._model = value;
    }

    public get table():QuandlTable {
        return this._table;
    }

    public set table(value:QuandlTable) {
        this._table = value;
    }
}