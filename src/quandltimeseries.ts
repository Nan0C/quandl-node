import {RequestResponse} from "request";
import {Quandlmodel} from "./quandlmodel";
import {fetch, RequestOptions} from "./request";

/**
 * Created by Soeren on 15.02.2018.
 */

export interface TimeSeriesMetadataOptions {
    dataset:string;
    database:string;
    format?:string;
}

export interface TimeSeriesDataOptions {
    dataset:string;
    database:string;
    format?:string;
    limit?:number;
    column_index?:number;
    start_date?:string;
    end_date?:string;
    order?:"asc"|"desc";
    collapse?:"none"|"daily"|"weekly"|"monthly"|"quarterly"|"annual";
    transform?:"none"|"diff"|"rdiff"|"rdiff_from"|"cumul"|"normalize";
}

export interface TimeSeriesDatabaseMetadataOptions {
    database:string;
    format?:string;
}

export class QuandlTimeseries {
    private model:Quandlmodel;

    constructor(model:Quandlmodel){
        this.model = model;
    }

    /**
     * Returns timeSeries data
     * @param {TimeSeriesDataOptions} options
     * @returns {Promise<request.RequestResponse>}
     */
    public async data(options:TimeSeriesDataOptions):Promise<RequestResponse>{
        let config:RequestOptions = {
            uri: [this.model.uri,"api",this.model.api_version,'datasets',options.database,options.dataset,"data."+(options.format||"json")].join('/'),
            qs: {
                api_key: this.model.api_key,
                limit: options.limit,
                column_index: options.column_index,
                start_date: options.start_date,
                end_date: options.end_date,
                order: options.order,
                collapse: options.collapse,
                transform: options.transform
            }
        };
        return fetch(config,this.model);
    }

    /**
     * Get TimeSeries Data and Metadata
     * @param {TimeSeriesDataOptions} options
     * @returns {Promise<request.RequestResponse>}
     */
    public async all(options:TimeSeriesDataOptions):Promise<RequestResponse>{
        let config:RequestOptions = {
            uri: [this.model.uri,"api",this.model.api_version,'datasets',options.database,options.dataset+"."+(options.format||"json")].join('/'),
            qs: {
                api_key: this.model.api_key,
                limit: options.limit,
                column_index: options.column_index,
                start_date: options.start_date,
                end_date: options.end_date,
                order: options.order,
                collapse: options.collapse,
                transform: options.transform
            }
        };
        return fetch(config,this.model);
    }

    /**
     * Get Metadata for a Database
     * @param options
     * @returns {Promise<request.RequestResponse>}
     */
    public async metadata(options:TimeSeriesDatabaseMetadataOptions):Promise<RequestResponse>{
        let config:RequestOptions = {
            uri: [this.model.uri,"api",this.model.api_version,'databases',options.database+"."+(options.format||"json")].join('/'),
            qs: {
                api_key: this.model.api_key
            }
        };
        return fetch(config,this.model);
    }
}