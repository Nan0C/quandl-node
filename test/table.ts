import * as chai from "chai";
import {Quandl} from "../lib";

describe('TableQuandlData', function() {
    let quandl:Quandl;
    this.timeout(0);

    beforeEach(() => {
        quandl = new Quandl({
            key:""
        });
    });

    afterEach(() => {

    });

    describe('#Fetch table data', () => {
        it('Requests table data from quandl, from the ETFG/FUND table', async () => {
            let data:any = await quandl.table.rows({
                publisher:"ETFG",
                datatable:"FUND",
                filter: {
                    ticker:"SPY"
                }
            });
            chai.should().exist(data,'Failed fetching table data from quandl');
            data = JSON.parse(data);
            chai.should().exist(data.datatable);
            chai.should().exist(data.datatable.data);
            chai.should().exist(data.datatable.columns);
            chai.expect(data.datatable.data[0][1]).to.equal("SPY");
        });

        it("Filters the Column ticker for the ETFG/FUND table", async () => {
            let data:any = await quandl.table.rows({
                publisher:"ETFG",
                datatable:"FUND",
                qopts: {
                    columns: "ticker"
                }
            });
            chai.should().exist(data,'Failed fetching table data from quandl');
            data = JSON.parse(data);
            chai.should().exist(data.datatable);
            chai.should().exist(data.datatable.data);
            chai.should().exist(data.datatable.columns);
            chai.expect(data.datatable.data[0].length).to.equal(1);
            chai.expect(data.datatable.columns[0].name).to.equal("ticker");
        });

        it("Get Table Metadata for AR/MWCS", async () => {
           let data:any = await quandl.table.metadata({
               publisher:"AR",
               datatable:"MWCS"
           });
           chai.should().exist(data,'Failed to fetch metadata for table AR/MWSC');
           data = JSON.parse(data);
           chai.should().exist(data.datatable);
           chai.expect(data.datatable.vendor_code).to.equal("AR");
           chai.expect(data.datatable.datatable_code).to.equal("MWCS");
           chai.should().exist(data.datatable.name);
        });
    });
});