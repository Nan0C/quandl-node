import * as chai from "chai";
import {Quandl} from "../lib";


describe('TimeseriesQuandlData', function() {
    let quandl:Quandl;
    this.timeout(0);

    beforeEach(() => {
        quandl = new Quandl({
            key:""
        });
    });

    afterEach(() => {

    });

    describe('#Fetch timeseries data', () => {
        it('Requests Timeseries data from quandl, from the DB: WIKI for FB', async () => {
            let data:any = await quandl.timeseries.data({
                database:"WIKI",
                dataset:"FB"
            });
            chai.should().exist(data,"Fetching Timeseries data failed");
            data = JSON.parse(data);
            let dataset = data.dataset_data;
            chai.should().exist(dataset.start_date);
            chai.should().exist(dataset.end_date);
            chai.should().exist(dataset.data);
            console.log(data);
        });

        it('Request Filtered timeseries data', async () => {
            let data:any = await quandl.timeseries.data({
                database:"WIKI",
                dataset:"FB",
                start_date:"2017-06-12",
                end_date: "2017-12-12",
                order:"asc",
                limit:15,
            });
            chai.should().exist(data,"Fetching filtered Timeseries data failed");
            data = JSON.parse(data);
            let dataset = data.dataset_data;
            chai.expect(dataset.start_date).to.equal("2017-06-12");
            chai.expect(dataset.end_date).to.equal("2017-12-12");
            chai.expect(dataset.order).to.equal("asc");
            chai.expect(dataset.limit).to.equal(15);
            chai.expect(dataset.data.length).to.equal(15);
            chai.should().exist(dataset.data);
        });

        it("Request Filtered Timeseries Data and Metadata", async () => {
            let data:any = await quandl.timeseries.all({
                database:"WIKI",
                dataset:"FB",
                start_date:"2017-06-12",
                end_date: "2017-12-12",
                order:"asc",
                limit:15,
            });
            chai.should().exist(data,"Fetching filtered Timeseries data failed");
            data = JSON.parse(data);
            let dataset = data.dataset;
            chai.expect(dataset.start_date).to.equal("2017-06-12");
            chai.expect(dataset.end_date).to.equal("2017-12-12");
            chai.expect(dataset.order).to.equal("asc");
            chai.expect(dataset.limit).to.equal(15);
            chai.expect(dataset.data.length).to.equal(15);
            chai.should().exist(dataset.data);
            chai.expect(dataset.database_code).to.equal("WIKI");
            chai.expect(dataset.dataset_code).to.equal("FB");
            chai.should().exist(dataset.description);
        });

        it("Fetch Metadata for a timeseries Database", async () => {
            let data:any = await quandl.timeseries.metadata({
                database:"WIKI"
            });
            chai.should().exist(data,"Fetching Metadata for Database failed");
            data = JSON.parse(data);
            let database = data.database;
            chai.expect(database.id).to.equal(4922);
            chai.expect(database.database_code).to.equal("WIKI");
        })
    });
});