"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const lib_1 = require("../lib");
describe('TableQuandlData', function () {
    let quandl;
    this.timeout(0);
    beforeEach(() => {
        quandl = new lib_1.Quandl({
            key: ""
        });
    });
    afterEach(() => {
    });
    describe('#Fetch table data', () => {
        it('Requests table data from quandl, from the ETFG/FUND table', () => __awaiter(this, void 0, void 0, function* () {
            let data = yield quandl.table.rows({
                publisher: "ETFG",
                datatable: "FUND",
                filter: {
                    ticker: "SPY"
                }
            });
            chai.should().exist(data, 'Failed fetching table data from quandl');
            data = JSON.parse(data);
            chai.should().exist(data.datatable);
            chai.should().exist(data.datatable.data);
            chai.should().exist(data.datatable.columns);
            chai.expect(data.datatable.data[0][1]).to.equal("SPY");
        }));
        it("Filters the Column ticker for the ETFG/FUND table", () => __awaiter(this, void 0, void 0, function* () {
            let data = yield quandl.table.rows({
                publisher: "ETFG",
                datatable: "FUND",
                qopts: {
                    columns: "ticker"
                }
            });
            chai.should().exist(data, 'Failed fetching table data from quandl');
            data = JSON.parse(data);
            chai.should().exist(data.datatable);
            chai.should().exist(data.datatable.data);
            chai.should().exist(data.datatable.columns);
            chai.expect(data.datatable.data[0].length).to.equal(1);
            chai.expect(data.datatable.columns[0].name).to.equal("ticker");
        }));
        it("Get Table Metadata for AR/MWCS", () => __awaiter(this, void 0, void 0, function* () {
            let data = yield quandl.table.metadata({
                publisher: "AR",
                datatable: "MWCS"
            });
            chai.should().exist(data, 'Failed to fetch metadata for table AR/MWSC');
            data = JSON.parse(data);
            chai.should().exist(data.datatable);
            chai.expect(data.datatable.vendor_code).to.equal("AR");
            chai.expect(data.datatable.datatable_code).to.equal("MWCS");
            chai.should().exist(data.datatable.name);
        }));
    });
});
